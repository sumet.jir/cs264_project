const http = require('http');
const fs = require('fs');
const express = require("express");
const path = require('path');
const { urlencoded, request } = require('express');
const app = express();
const mysql = require('mysql');
const ejs = require('ejs');
var bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const multer = require('multer');
const user = require('./login3');


var n;
var name;

//environments
//static page
app.use(express.static(__dirname));
//Middleware
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(fileUpload());


var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    var dir = './uploads';

    //if not have dir folder, then create it.
    if(!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    callback(null, dir);
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});


var upload = multer({ storage : storage }).array('myfile', 15);

var file;

//create database for store students data
var con = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "Password@123",
  database:"students_data"
});

//connect to the data base
con.connect( (err) => {
  if (err) throw err;
  console.log("Connected Database!");
 
});

app.use('/u1', (req,res) => {
  res.sendFile(path.join(__dirname+'/Userstory1.html'));
})

app.use('/main', (req,res) => {
    res.sendFile(path.join(__dirname+'/264-Main.html'));
})

//if use post, data in index2 will disappear

app.use('/index2?', (req,res) => {
  
  file = req.files;
  upload(req,res ,function(err) {
    if(err) {
      console.log("can't upload file");
    }else {
      console.log("succ!");
      
    }
  });
  
 
  res.sendFile(path.join(__dirname+'/index2.html'));
});

app.get('/searchInfo', (req,res) => {

  con.query("SELECT * FROM student_data", (err,rows) => {
    if(err) throw err;
    res.render('search', { user:rows });
  })

})


//access url path login page
app.get('/login',(req,res) => {
    res.sendFile(path.join(__dirname+'/login.html'));
    
});


//access url path form page
app.get('/form', (req,res ) => {
  
    res.sendFile(path.join(__dirname+'/index.html'));
})

//access url path contact page
app.get('/contact', (req,res ) => {
  res.sendFile(path.join(__dirname+'/contact.html'));
  
})

//display student processing list
app.get('/list', (req,res) => {

    con.query("SELECT * FROM student_data WHERE student_id= ?", [n], (err,rows) => {
      if(err) throw err;
      res.render('u12', {title:name , user:rows });
    })

  
})

app.get('/search', (req,res) => {
  con.query("SELECT * FROM student_data WHERE student_id= ?", [n], (err,rows) => {
    if(err) throw err;
    
    res.render('info', {user:rows });
  })
})

app.get('/professor', (req,res) => {
  res.render('professor');
})

//display data which users input and insert data from html form into mysql
app.post('/submit' , (req,res) => {

  n = req.body.stdId;
  name= req.body.stdName;
  console.log(req.body);
  

  
  res.send(req.body);

  console.log(file);

  var sql = "INSERT INTO student_data (name, student_id, year, field, addressNumber, moo, tumbol, amphur, province, postNumber, mobilePhone, phone, professor,mail , addNumber, withdrawNumber, scode1, sname1, sec1, date1, weight1, sprofessor1, studentChoice1, scode2, sname2, sec2, date2, weight2, sprofessor2, studentChoice2, scode3, sname3, sec3, date3, weight3, sprofessor3, studentChoice3, scode4, sname4, sec4, date4, weight4, sprofessor4, studentChoice4, scode5, sname5, sec5, date5, weight5, sprofessor5, studentChoice5, scode6, sname6, sec6, date6, weight6, sprofessor6, studentChoice6, scode7, sname7, sec7, date7, weight7, sprofessor7, studentChoice7, scode8, sname8, sec8, date8, weight8, sprofessor8, studentChoice8, scode9, sname9, sec9, date9, weight9, sprofessor9, studentChoice9, scode10, sname10, sec10, date10, weight10, sprofessor10, studentChoice10, file, reason, status, date) VALUES ('"+req.body.stdName+"', '"+req.body.stdId+"', '"+req.body.stdYear+"', '"+req.body.stdFiels+"', '"+req.body.addressNumber+"', '"+req.body.moo+"', '"+req.body.tumbol+"', '"+req.body.amphur+"', '"+req.body.province+"', '"+req.body.postalCode+"', '"+req.body.mobilePhone+"', '"+req.body.phone+"', '"+req.body.advisor+"','"+req.body.mail+"' ,'"+req.body.add+"', '"+req.body.drop+"', '"+req.body.scode1+"' , '"+req.body.nsub1+"' , '"+req.body.sec1+"' , '"+req.body.date1+"' , '"+req.body.cd1+"', '"+req.body.tn1+"', '"+req.body.adddrop1+"' , '"+req.body.scode2+"' , '"+req.body.nsub2+"' , '"+req.body.sec2+"' , '"+req.body.date2+"' , '"+req.body.cd2+"', '"+req.body.tn2+"', '"+req.body.adddrop2+"' , '"+req.body.scode3+"' , '"+req.body.nsub3+"' , '"+req.body.sec3+"' , '"+req.body.date3+"' , '"+req.body.cd3+"', '"+req.body.tn3+"', '"+req.body.adddrop3+"' , '"+req.body.scode4+"' , '"+req.body.nsub4+"' , '"+req.body.sec4+"' , '"+req.body.date4+"' , '"+req.body.cd4+"', '"+req.body.tn4+"', '"+req.body.adddrop4+"' , '"+req.body.scode5+"' , '"+req.body.nsub5+"' , '"+req.body.sec5+"' , '"+req.body.date5+"' , '"+req.body.cd5+"', '"+req.body.tn5+"', '"+req.body.adddrop5+"' , '"+req.body.scode6+"' , '"+req.body.nsub6+"' , '"+req.body.sec6+"' , '"+req.body.date6+"' , '"+req.body.cd6+"', '"+req.body.tn6+"', '"+req.body.adddrop6+"' , '"+req.body.scode7+"' , '"+req.body.nsub7+"' , '"+req.body.sec7+"' , '"+req.body.date7+"' , '"+req.body.cd7+"', '"+req.body.tn7+"', '"+req.body.adddrop7+"' , '"+req.body.scode8+"' , '"+req.body.nsub8+"' , '"+req.body.sec8+"' , '"+req.body.date8+"' , '"+req.body.cd8+"', '"+req.body.tn8+"', '"+req.body.adddrop8+"' , '"+req.body.scode9+"' , '"+req.body.nsub9+"' , '"+req.body.sec9+"' , '"+req.body.date9+"' , '"+req.body.cd9+"', '"+req.body.tn9+"', '"+req.body.adddrop9+"' , '"+req.body.scode10+"' , '"+req.body.nsub10+"' , '"+req.body.sec10+"' , '"+req.body.date10+"' , '"+req.body.cd10+"', '"+req.body.tn10+"', '"+req.body.adddrop10+"', '"+req.body.myfile+"' , '"+req.body.reason+"', 'กำลังดำเนินการ' , '"+req.body.day+"')";

  con.query(sql, (err) => {
    if(err) throw err;

    console.log("insert data into database");
  })

  
})

app.post('/index.html', (req,res) => {
  res.sendFile(path.join(__dirname+'/index.html'));
})


  
  //add the router
app.listen(5500);
console.log('Running at Port 5500');